# GHG Consumption page

Read LHC Gas Systems consumptions from NXCALS and perform basic
analytics

```
docker build ghg-consumption .
```

```
docker run -ti -p 5000-5500:5000-5500  \
-v $(pwd)/ghgstudy.keytab:/mnt/acclog/private.keytab  \
-e KPRINCIPAL=ghgstudy \
-v $(pwd)/src:/opt/nxcals-spark/src \
-v $(pwd)/spark-env.sh:/opt/nxcals-spark/nxcals-bundle/conf/spark-env.sh \
-v $(pwd)/spark-defaults.conf:/opt/nxcals-spark/nxcals-bundle/conf/spark-defaults.conf \
-v $(pwd)/data:/data \
-e PYSPARK_PYTHON=/opt/nxcals-spark/bin/python \
--net nxcals-network \
-e SPARK_LOCAL_IP="0.0.0.0" \
-e SPARK_PUBLIC_DNS="yourmachinename.cern.ch" \
-e SPARK_DRIVER_HOST="yourmachinename.cern.ch" \
ghg-consumption-test
```