from nxcals.spark_session_builder import get_or_create, Flavor
from nxcals.api.extraction.data.builders import DataQuery
from pyspark.sql.functions import pandas_udf, PandasUDFType
from pyspark.sql import Window
import pandas as pd
import pyspark.sql.functions as f
import os
import logging
import psycopg2
import json
import re
import time
from sqlalchemy import create_engine, text

logging.basicConfig(level=logging.INFO, 
                    format='%(asctime)s - %(levelname)s - %(message)s')
print("Starting script")

# Extract:
# first three letters => experiment
# remaining letters until _ => detector
# [1,2,3,4] => line number
# [6, 9] => low flow or high flow
device_regex_pattern = r"([A-Z]{3})([A-Z]+)_MX_XMFC1([1234])0([69])\.POSST"
compiled_pattern = re.compile(device_regex_pattern)


def run_etl():
    try:
        dbname = os.environ['POSTGRES_DB']
        user = os.environ['POSTGRES_USER']
        password = os.environ['POSTGRES_PASSWORD']
        host = os.environ['POSTGRES_HOST']
        port = os.environ['POSTGRES_PORT']
        conn = psycopg2.connect(dbname=dbname, user=user, password=password, host=host, port=port, sslmode="disable", gssencmode="disable")
        engine = create_engine(f'postgresql+psycopg2://{user}:{password}@{host}:{port}/{dbname}?sslmode=disable')
        print("Loaded DB configuration")

        # Load configuration
        with open('config.json') as config_file:
            config = json.load(config_file)

        if 'variables' not in config or 'data_retrieval' not in config:
            logging.error("Config file missing required sections.")
            exit(1)  # Exit if config is not correctly formatted
            
        print("Loaded config.json configuration")
            
        # Read the MFC from config and store the parsed values in the DB
        device_info = []

        for device in config['variables']:
            device_name = device.get('name')
            gas = device.get('gas')
            match = re.match(compiled_pattern, device_name)
            if match:
                experiment, detector, line, flow_type = match.groups()
                flow_type = 'low' if flow_type == '6' else 'high'
                full_name_experiment = config['gas_systems_naming'].get(experiment)
                device_info.append((device_name, full_name_experiment, detector, gas))

        print("Parsed device info: %s", device_info)

        with conn.cursor() as cursor:
            upsert_query = '''
            INSERT INTO mass_flow_controllers_metadata (mass_flow_controller_id, experiment, detector, gas)
            VALUES (%s, %s, %s, %s)
            ON CONFLICT (mass_flow_controller_id) DO UPDATE SET 
            experiment = EXCLUDED.experiment,
            detector = EXCLUDED.detector,
            gas = EXCLUDED.gas
            '''
            try:
                for info in device_info:
                    cursor.execute(upsert_query, info)
                conn.commit()
            except Exception as e:
                conn.rollback()
                logging.exception("Failed to insert data: %s", e)
                

        keytab = os.environ.get('KEYTAB_PATH', "/mnt/acclog/private.keytab")
        spark = get_or_create(app_name='GHGSTUDY_APP', flavor=Flavor.YARN_MEDIUM,
                            conf={
                                'spark.driver.extraJavaOptions':
                                    f'''-Dservice.url=https://cs-ccr-nxcals5.cern.ch:19093,https://cs-ccr-nxcals5.cern.ch:19094,https://cs-ccr-nxcals6.cern.ch:19093,https://cs-ccr-nxcals6.cern.ch:19094,https://cs-ccr-nxcals7.cern.ch:19093,https://cs-ccr-nxcals7.cern.ch:19094,https://cs-ccr-nxcals8.cern.ch:19093,https://cs-ccr-nxcals8.cern.ch:19094 
                                    -Dkerberos.principal={os.environ['KPRINCIPAL']} -Dkerberos.keytab={keytab}''',
                                'spark.kerberos.keytab': keytab,
                                'spark.kerberos.principal': os.environ['KPRINCIPAL'],
                                # 'spark.driver.bindAddress': os.environ['SPARK_LOCAL_IP'],
                                'spark.driver.host': os.environ['SPARK_DRIVER_HOST'],
                                'spark.driver.maxResultSize': '1t',
                                'spark.task.maxDirectResultSize': '1t'
                            }
                            )

        time_start = config['data_retrieval']['start_date']
        time_stop = config['data_retrieval']['end_date']
        tstart, tstop = pd.to_datetime(time_start), pd.to_datetime(time_stop)

        variables = [item["name"] for item in config['variables']]

        # Step 1 - Retrieve the variables. The dataframe has timestamp, variable name and value
        sparkdf = DataQuery.getForVariables(spark, system="WINCCOA", start_time=tstart, end_time=tstop,
                                        variables_like=variables)
        # Convert the timestamp from epoch seconds to a spark timestamp object
        sparkdf = sparkdf.withColumn('time', (f.col('nxcals_timestamp') / 1e9).cast("timestamp"))
        # Remove useless columns 
        sparkdf = sparkdf.drop('nxcals_entity_id', 'application_arcgroup', 'nxcals_timestamp')
        # Sort columns for proper aggregation and integration later
        sparkdf = sparkdf.sort("nxcals_variable_name", "time")

        # Use a window function to compute the integral of the flow for each device
        window = Window.partitionBy("nxcals_variable_name").orderBy("time")
        # This calculates the delta t between the next point and the current one
        time_diff = (f.lead("time", 1).over(window).cast("long") - f.col("time").cast("long"))
        # Multiply deltat by the current value
        sparkdf = sparkdf.withColumn("integral", f.col("nxcals_value") * time_diff * 1 / 3600)
        # Group by variable name and day and compute the volume for that day
        sparkdf = sparkdf.groupby(["nxcals_variable_name", sparkdf['time'].cast('date').alias('date')]).agg(f.sum("integral").alias("volume"))

        # Add a column with the cumulative sum of the volume consumed
        windowSpec = Window.partitionBy('nxcals_variable_name').orderBy('date').rowsBetween(Window.unboundedPreceding, 0)
        sparkdf = sparkdf.withColumn('cumulative_volume', f.sum('volume').over(windowSpec))
        sparkdf = sparkdf.withColumnRenamed('nxcals_variable_name', 'mass_flow_controller_id')

        # Calculate the weekly consumption
        weekly_df = sparkdf.withColumn('week', f.weekofyear('date')).withColumn('year', f.year('date'))
        weekly_agg = weekly_df.groupBy(['mass_flow_controller_id', 'year', 'week']).agg(f.sum('volume').alias('total_weekly_volume'))


        # Create a DataFrame with all weeks of the year for each distinct year in your data
        years_df = weekly_df.select('year').distinct()
        weeks_df = spark.range(1, 53).toDF("week")  # Weeks from 1 to 52
        months_df = spark.range(1, 13).toDF("month")  # Weeks from 1 to 12
        year_week_df = years_df.crossJoin(weeks_df)
        year_month_df = years_df.crossJoin(months_df)

        # Get all distinct mass_flow_controller_ids
        controller_ids_df = weekly_df.select('mass_flow_controller_id').distinct()
        # Cross join to get all combinations of mass_flow_controller_id, year and week
        all_combinations_df = controller_ids_df.crossJoin(year_week_df)
        # Left join with your weekly_agg to get all rows with total_weekly_volume where available
        weekly_df_with_all_weeks = all_combinations_df.join(
            weekly_agg, 
            ["mass_flow_controller_id", "year", "week"], 
            "left"
        )
        # Replace nulls with zero or some default value in total_weekly_volume if needed
        weekly_df_with_all_weeks = weekly_df_with_all_weeks.fillna({'total_weekly_volume': 0})

        # Calculate the monthly consumption
        monthly_df = sparkdf.withColumn('month', f.month('date')).withColumn('year', f.year('date'))
        monthly_agg = monthly_df.groupBy(['mass_flow_controller_id', 'year', 'month']).agg(f.sum('volume').alias('total_monthly_volume'))

        # Get all distinct mass_flow_controller_ids
        controller_ids_df = monthly_df.select('mass_flow_controller_id').distinct()
        # Cross join to get all combinations of mass_flow_controller_id, year and week
        all_combinations_df = controller_ids_df.crossJoin(year_month_df)
        # Left join with your weekly_agg to get all rows with total_weekly_volume where available
        monthly_df_with_all_weeks = all_combinations_df.join(
            monthly_agg, 
            ["mass_flow_controller_id", "year", "month"], 
            "left"
        )
        # Replace nulls with zero or some default value in total_weekly_volume if needed
        monthly_df_with_all_weeks = monthly_df_with_all_weeks.fillna({'total_monthly_volume': 0})


        # Convert to Pandas DataFrames for insertion into PostgreSQL
        weekly_pandasdf = weekly_df_with_all_weeks.toPandas().sort_values(by=['mass_flow_controller_id', 'year', 'week'])
        monthly_pandasdf = monthly_df_with_all_weeks.toPandas().sort_values(by=['mass_flow_controller_id', 'year', 'month'])
        print("Monthly data retrieved: %d rows", monthly_pandasdf.iloc[-5:])
        print("Weekly data retrieved: %d rows", weekly_pandasdf.iloc[-5:])

        pandasdf = sparkdf.toPandas().rename(columns={'volume': 'daily_gas_volume', 'cumulative_volume': 'total_gas_volume'})
        print("Retrieved %d rows and %d columns", pandasdf.shape[0], pandasdf.shape[1])

        with conn.cursor() as cur:
            cur.execute('TRUNCATE mass_flow_controllers_data RESTART IDENTITY;')
            cur.execute('TRUNCATE weekly_consumptions RESTART IDENTITY;')
            cur.execute('TRUNCATE mass_flow_controllers_data RESTART IDENTITY;')
            print("Deleting previous data")

            # Save data to DB
            pandasdf.to_sql('mass_flow_controllers_data', engine, if_exists='append', index=False)
            weekly_pandasdf.to_sql('weekly_consumptions', engine, if_exists='append', index=False)
            monthly_pandasdf.to_sql('monthly_consumptions', conn, if_exists='replace', index=False)
            conn.commit()

        conn.close()

    except Exception as e:
        print("An exception occurred when running the script")
        print(e)


if __name__ == "__main__":
    while True:
        run_etl()
        time.sleep(3600)