# Note: set USER and TOKEN env variables for building the image

# Use a acc alma linux images
ARG FROM=gitlab-registry.cern.ch/acc-co/docker/acc-base-images/acc_al9_jdk8:latest
FROM $FROM

# Install necessary packages
RUN dnf makecache && \
    dnf install -y krb5-workstation krb5-libs git unzip bsdtar tar rsync CERN-CA-certs which glibc && \
    dnf clean all && \
    curl -o /etc/krb5.conf https://linux.web.cern.ch/docs/krb5.conf


# Setup Spark installation directory and permissions
ENV SPARK_INSTALL=/opt/nxcals-spark
RUN mkdir -p $SPARK_INSTALL && \
    mkdir -p $SPARK_INSTALL/work && \
    chmod 777 $SPARK_INSTALL/work 

RUN adduser ghgstudy -m

RUN cd $SPARK_INSTALL && \
    python -m venv . && \
    chown -R ghgstudy $SPARK_INSTALL 

# Switch to non-root user for remaining operations
USER ghgstudy
COPY requirements.txt ${SPARK_INSTALL}
RUN cd $SPARK_INSTALL && \
    source $SPARK_INSTALL/bin/activate && \
    pip install git+https://${USER}:${TOKEN}@gitlab.cern.ch/acc-co/devops/python/acc-py-pip-config && \
    pip list && \
    pip install -r requirements.txt

# Define volumes for external data and source code
VOLUME $SPARK_INSTALL/src

# Set the working directory
WORKDIR $SPARK_INSTALL

# Set environment variables for Spark
ENV SPARK_HOME=${SPARK_INSTALL}/nxcals-bundle
ENV PYSPARK_DRIVER_PYTHON=/opt/nxcals-spark/bin/ipython
ENV PYSPARK_PYTHON=/opt/nxcals-spark/bin/python
ENV SPARK_LOCAL_IP="0.0.0.0"
# ENV SPARK_PUBLIC_DNS
# ENV SPARK_DRIVER_HOST

# Copy the entrypoint script and source code
USER root
COPY entrypoint.sh /
COPY src/ src/
RUN chmod 755 /entrypoint.sh

USER ghgstudy
ENTRYPOINT ["/entrypoint.sh"]
CMD ["spark-submit", "ghg-consumptions/src/main.py"]
